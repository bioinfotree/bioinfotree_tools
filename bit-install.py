#!/usr/bin/env python
#
# Copyright 2010-2011 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010-2011 Paolo Martini <paolo.cavei@gmail.com>

from distutils.sysconfig import get_python_inc
from optparse import OptionParser
from os import chdir, environ, getuid, makedirs
from os.path import abspath, exists, isdir, join, split
from pkg_resources import get_distribution, DistributionNotFound
from pwd import getpwuid
from socket import getfqdn
from subprocess import Popen, PIPE
from sys import exit, modules
import re

welcome_msg = '''
### Welcome to the BioinfoTree installer ###

This script will perform the guided installation of the
BioinfoTree software suite on your computer.

The installer  will check your system looking  for some
software needed by  BioinfoTree.  Then it will download
all BioinfoTree files and will compile some utilities.

Finally it will optionally update  your bash profile in
order to enable BioinfoTree tools.

Please press ENTER to continue or CTRL+C to abort.
'''

ending_msg = '''
The  BioinfoTree core was succesfully installed on your
system.

=======================================================
*                                                     *
*            BioinfoTree is (almost) ready.           *
*                                                     *
=======================================================

To complete the installation,  please add the following
lines:

[[ -z $BIOINFO_ROOT ]] && export BIOINFO_ROOT=%s
export BIOINFO_HOST=%s
source $BIOINFO_ROOT/local/share/bash/bashrc

to the top of $HOME/.bash_profile or $HOME/.bashrc

Please be aware that such commands must be executed both
in interactive and in non-interactive sessions.
This means that they must be placed before checks like:

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

For more help please visit:

http://bioinfotree.org

Thank you and enjoy the BioinfoTree!
'''

name_msg = 'Your name [%s]: '

email_msg = 'Your email address: '

installation_path_msg = 'Installation path [%s]: '

hostname_msg = 'Unique host name [%s]: '

deps = {
    'standalone' : [ ('bash', '3'),
                     ('git', '1.7'),
                     ('gawk', '3.1'),
                     ('sed', '4.2'),
                     ('perl', '5.10'),
                     ('python', '2.5'),
                     ('R', '2.10'),
                     ('cython', '0.11') ],

    'python'     : [ ('numpy', '1.3'),
                     ('scipy', '0.7') ],

    'R'          : [ ('optparse', '0') ]
}

installable = { 'pyvfork library'     : 'local/src/pyvfork',
                'R support libraries' : 'local/src/Rvfork' }

def get_input(msg, default=None):
    while 1:
        reply = raw_input(msg)
        print
        
        if len(reply) == 0:
            if default is None:
                continue
            else:
                return default
        else:
            return reply

def check_version(expected, found):
    for e, f in zip(expected, found):
        if e < f:
            return True
        elif e > f:
            return False
    return True


version_rx = re.compile(r'(\d+)\.(\d+)')

def check_standalone(lst):
    for prog, version in lst:
        try:
            proc = Popen([prog, '--version'], stdout=PIPE, stderr=PIPE, close_fds=True, shell=False)
            out, err = proc.communicate()
            if proc.returncode != 0:
                exit('Error checking "%s" version.' % prog)
            
            lines = (out+err).split('\n')
            while len(lines) and len(lines[0]) == 0:
                del lines[0]

            if len(lines) == 0:
                exit('Error checking "%s" version.' % prog)

            m = version_rx.search(lines[0])
            if m is None:
                exit('Error checking version of "%s".' % prog)

            min_version = tuple( int(n) for n in version.split('.') )
            found_version = tuple( int(n) for n in m.groups() )
            if not check_version(min_version, found_version):
                exit('Program "%s" is too old. Please install version %s or higher.' % (prog,version))

        except OSError:
            exit('Program "%s" not found. Please install version %s or higher.' % (prog,version))

def check_python(lst):
    python_header = join(get_python_inc(), 'Python.h')
    if not exists(python_header):
        exit('Python header files are missing. Please install them.')

    for name, version in lst:
        try:
            distrib = get_distribution(name)
        except DistributionNotFound:
            exit('Python module "%s" not found. Please install version %s or higher.' % (name,version))

        m = version_rx.search(distrib.version)
        if m is None:
            exit('Error checking version of Python module "%s".' % name)

        min_version = tuple( int(n) for n in version.split('.') )
        found_version = tuple( int(n) for n in m.groups() )
        if not check_version(min_version, found_version):
            exit('Python module "%s" is too old. Please install version %s or higher.' % (name,version))


def git_clone(repository, root):
    proc = Popen(['git', 'clone', repository, root], shell=False)
    if proc.wait() != 0:
        exit('ERROR: Cannot clone remote BioinfoTree repository at "%s".' % repository)
    print

def git_user(name, email):
    proc = Popen(['git', 'config', 'user.name', name], shell=False)
    if proc.wait() != 0:
        exit('ERROR: Cannot configure git "user.name".')

    proc = Popen(['git', 'config', 'user.email', email], shell=False)
    if proc.wait() != 0:
        exit('ERROR: Cannot configure git "user.email".')

def install_pkgs(root, pkgs):
    cmd = 'source $BIOINFO_ROOT/local/share/bash/bashrc; binary_install'

    for name, path in pkgs.iteritems():
        proc = Popen(['/bin/bash', '-c', cmd], shell=False, cwd=join(root, path))
        if proc.wait() != 0:
            exit('ERROR: Cannot install %s.' % name)
    print


def main():
    parser = OptionParser(usage='%prog [OPTIONS]')
	# previous default was: bitro@bioinfotree.org:bioinfotree.git
    parser.add_option('-r', '--repository', dest='repository', default='bitro@bioinfotree.org:sales.git', help='define the BioinfoTree repository (default: %default)')
    options, args = parser.parse_args()

    if len(args) != 0:
        exit('Unexpected argument number.')

    check_standalone(deps['standalone'])
    check_python(deps['python'])
    
    user_info = getpwuid(getuid())

    get_input(welcome_msg, '')

    default_name = user_info.pw_gecos.rstrip(',') 
    name = get_input(name_msg % default_name, default_name)

    email = get_input(email_msg)

    default_root = join(user_info.pw_dir, 'bioinfotree')
    root = get_input(installation_path_msg % default_root, default_root)
    root = abspath(root)
    if exists(root):
        exit('ERROR: The path "%s" already exists.' % root)

    root_prefix, root_name = split(root)
    if not isdir(root_prefix):
        exit('ERROR: The directory "%s" does not exist.' % root_prefix)
    
    default_hostname = getfqdn()
    hostname = get_input(hostname_msg % default_hostname, default_hostname)

    try:
        chdir(root_prefix)
    except:
        exit('ERROR: Cannot enter directory "%s".' % root_prefix)
    git_clone(options.repository, root_name)

    try:
        chdir(root)
    except:
        exit('ERROR: Cannot enter directory "%s".' % root)
    git_user(name, email)
    
    environ['BIOINFO_ROOT'] = root
    environ['BIOINFO_HOST'] = hostname
    makedirs(join(root, "binary", hostname, "local", "lib", "R"))
    install_pkgs(root, installable)

    print ending_msg % (root, hostname)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit('\nInstallation aborted.')
        
