#!/usr/bin/env python

#
# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from argparse import ArgumentParser, RawDescriptionHelpFormatter
from vfork.util import ignore_broken_pipe
from vfork.util import exit, format_usage
import os
from subprocess import Popen, PIPE
from warnings import warn
from sys import stdout

default_remote_name   = "sales"
default_remote_url    = "bitro@bioinfotree.org:sales.git"
default_remote_branch = "master"



# find executable in linux or mac
def which(program):
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    def ext_candidates(fpath):
        yield fpath
        for ext in os.environ.get("PATHEXT", "").split(os.pathsep):
            yield fpath + ext

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            for candidate in ext_candidates(exe_file):
                if is_exe(candidate):
                    return candidate

    return None

    
def format_out(str):
    for line in str.split("\n"):
      stdout.write("\t%s\n" % line.strip())


# execute command
def exec_cmd(cmd_lst,pwd=""):
	 pr = Popen(cmd_lst,                  
	        cwd=os.path.dirname(pwd), 
	        stdout=PIPE,
	        stderr=PIPE,
	        shell=False)
	 
	 stdout, stderr = pr.communicate()

	 print "[STDOUT " + " ".join(cmd_lst) + "]:"
	 format_out(stdout)
	 print "[STDERR " + " ".join(cmd_lst) + "]:"
	 format_out(stderr)
	 
	 return stdout, stderr
	 
	 
# get interactivelly input message
def get_input(msg, default=None):
    while 1:
        reply = raw_input(msg)
        print
        
        if len(reply) == 0:
            if default is None:
                continue
            else:
                return default
        else:
            return reply








def main():
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                            usage='%(prog)s [OPTIONS]',
                            description=format_usage('''
        Rebase all but work branches, on top of master Git repository.    
    '''))
    parser.add_argument("--repo", "-r", dest="repo", required=False, default=os.path.join(os.environ["BIOINFO_ROOT"],".git") ,help="Path of the directory containing the Git repository of BioinfoTree. Default: BIOINFO_ROOT")
    
    parser.add_argument("--remote_name", "-n", dest="remote_name", required=False, default=default_remote_name, help="Alias of remote repository. Default: %s" % default_remote_name)
    
    parser.add_argument("--remote_url", "-u", dest="remote_url", required=False, default=default_remote_url, help="URL of remote repository. Default: %s" % default_remote_url)
    
    args = parser.parse_args()
        
    # find git executable
    git_bin  = which("git")
    
    # get list of actual remote
    get_remote = [git_bin, "remote", "-v"]
    stdout, stderr = exec_cmd(get_remote,pwd=args.repo)
    
    # get name of remote containing sales_url
    try:
	remote_name = [line.split()[0] for line in stdout.split('\n') if sales_url + " (fetch)" in line][0]
    except:
	# if not, set sales urls
	set_remote = [git_bin, "remote", "add", args.remote_name, args.remote_url]
	stdout, stderr = exec_cmd(set_remote, pwd=args.repo)
	
    # gel list of branches
    get_branches = [git_bin, "branch"]
    branch_stdout, branch_stderr = exec_cmd(get_branches, pwd=args.repo)
    
    # search for presence of master branch
    try:
	master_branch = [line.split()[0] for line in branch_stdout.split('\n') if "master" in line][0]
    except RuntimeError:
	exit("master branch missing")

    # checkout on master branch
    co_master = [git_bin, "checkout", "master"]
    stdout, stderr = exec_cmd(co_master, pwd=args.repo)
 
    # pull from sales
    pull = [git_bin, "pull", "--rebase", args.remote_name, default_remote_branch]
    stdout, stderr = exec_cmd(pull, pwd=args.repo)
    
    # search for presence of work branch and remove it
    try:
	work_branch = [line.split()[0] for line in branch_stdout.split('\n') if "work" in line][0]
	rm_work = [git_bin, "branch", "-D", "work"]
	stdout, stderr = exec_cmd(rm_work, pwd=args.repo)
    except:
	warn("work branch missing", RuntimeWarning)
	
    
    # checkout on branches other than master and work, perform rebase on master
    for line in branch_stdout.split("\n"):
	if line and line.find("master") == -1 and line.find("work") == -1 and line.find("*") == -1:
	  
	    co_branch = [git_bin, "checkout", line.strip()]
	    stdout, stderr = exec_cmd(co_branch, pwd=args.repo)
	    
	    try:
		rebase_branch = [git_bin, "rebase", "master"]
		stdout, stderr = exec_cmd(rebase_branch, pwd=args.repo)
	    except NotImplementedError:
		pass
	      
    # return on master branch
    co_master = [git_bin, "checkout", "master"]
    stdout, stderr = exec_cmd(co_master, pwd=args.repo)
	      
    # create new work branch from master branch and checkout
    new_work = [git_bin, "checkout", "-b", "work"]
    stdout, stderr = exec_cmd(new_work, pwd=args.repo)
    
    
    # merge branches other than master and work itself
    for line in branch_stdout.split("\n"):
	if line and line.find("master") == -1 and line.find("work") == -1 and line.find("*") == -1:
	  	    
	    try:
		merge_branch = [git_bin, "merge", line.strip()]
		stdout, stderr = exec_cmd(merge_branch, pwd=args.repo)
	    except NotImplementedError:
		pass
		# copy from bit-install.py
	 
    return 0

    


    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit('\nUpdate aborted.')