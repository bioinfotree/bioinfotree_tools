#!/bin/bash
#
# sync_desk_to_laptop.sh --- 
# 
# Filename: sync_desk_to_laptop.sh
# Description: Synchronize files or directories from the remote host on the current guest. 
# Some directories are excluded from synchronization using exclusion rules in the file: 
# rsync_exclusion_rules.txt
#
# Author: Michele Vidotto
# Maintainer: 
# Created: Fri Jul 30 12:35:43 2010 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
#
# ------------- system commands used by this script --------------------
ID='/usr/bin/id';
ECHO='/bin/echo';

#MOUNT='/bin/mount';
#MKDIR='/bin/mkdir';
#RM='/bin/rm';
#MV='/bin/mv';
#CP='/bin/cp';
#TOUCH='/usr/bin/touch';

RSYNC='/usr/bin/rsync';

# ------------- backup configuration ----------------------------------

REMOTE_USER='michele';
REMOTE_HOST='147.162.172.2';

# Location of passphraseless ssh keyfile
RKEY='remote-private-key';

# Add all dirs or files you want to syncronize from the remote host, 
# separate by space
REMOTE_DIRS='/home/michele/bioinfotree';
LOCAL_DIR='/projects/novabreed/share/mvidotto';

# This option is related to the --exclude option, but it specifies a FILE that
# contains exclude patterns (one per line). Blank lines in the file and lines
# starting with ';' or '#' are ignored. If FILE is -, the list will be read 
# from standard input.
EXCLUDES_FILE='sync_bioinfotree_to_local_exclusion.txt';

#NUM_OF_SNAPSHOTS=7;
#BACKUP_INTERVAL=daily;


# ------------- the script itself --------------------------------------

$ECHO 'Starting of synchronization between Desktop -> Laptop on '`date`

# Check RSA key files
if [ ! -f $RKEY ]; then
  $ECHO "Couldn't find ssh keyfile!"
  $ECHO "Exiting..."
  exit 2
fi

# Check target directory on remote machine
if ! ssh -i $RKEY $REMOTE_USER@$REMOTE_HOST "test -x $RTARGET"; then
  $ECHO "Target directory on remote machine doesn't exist or bad permissions."
  $ECHO "Exiting..."
  exit 2
fi

# Check all directories in source machine
$ECHO "Verifying Sources..."
for source in $REMOTE_DIRS; do
	echo "Checking $source..."
	if [ ! -x $source ]; then
     $ECHO "Error with $source!"
     $ECHO "Directory either does not exist, or you do not have proper permissions."
#     exit 2
   fi
done

# Check exclusion file
if [! -f $EXCLUDES_FILE ]; then
    $ECHO "Ecluded file list does not exist!"
    exit 2
fi


$ECHO "Sources verified. Running rsync..."

for DIR in $REMOTE_DIRS; do


  # An explanation of above options to commands:
  #  --delete deletes files that don't exist on the system being backed up.(Optional)
  #  -a preserves the date and times, and permissions of the files (same as -rlptgoD).
  #  With this option rsync will:
  #          Descend recursively into all directories (-r),
  #          copy symlinks as symlinks (-l),
  #          preserve file permissions (-p),
  #          preserve modification times (-t),
  #          preserve groups (-g),
  #          preserve file ownership (-o), and
  #          preserve devices as devices (-D). 
  #
  #  -z compresses the data
  #  -vv increases the verbosity of the reporting process
  #
  #
  #  -e specifies remote shell to use
  #

  # /folder1 and folder2 In the examples above, folder1 and 2 are placeholders
  # for the directories to be synchronized. Folder1 is the original folder, 
  # and 2 is the new folder, or existing one to be brought in sync with the 
  # first. Replace them with the folders you'd like. A / was added after 
  # folder1 so that only the contents, rather than whole folder, would be 
  # moved into the second. 

  $RSYNC                                                                      \
          -azvv --delete --delete-excluded                                    \
          --exclude-from=$EXCLUDES_FILE                                       \
          -e "ssh -i $RKEY" $REMOTE_USER@$REMOTE_HOST:$DIR $LOCAL_DIR;

done

$ECHO 'Synchronization Desktop -> Laptop completed on '`date`;


# sudo rsync --delete -azvv -e ssh /home/path/folder1/ remoteuser@remotehost.remotedomain:/home/path/folder2

# rsync -azvv --delete --delete-excluded --exclude-from=/home/michele/Desktop/rsync_exclusion_rules.txt -e ssh michele@147.162.172.2:/home/michele/bioinfotree /home/michele/

# 
# sync_desk_to_laptop.sh ends here
