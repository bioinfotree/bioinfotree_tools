#!/bin/env bash

filename="$(basename $0)"
script_name="${filename%.*}.err"

>$script_name

echo -e "last run:\t$(date +'%d/%m/%Y')" >>$script_name
# sync private modules
rsync --delete -azvv -e ssh /projects/novabreed/share/privatemodules/* assembler:/mnt/vol1/projects/novabreed/share/privatemodules/ 2>>$script_name
# sync local installation
rsync --delete -azvv -e ssh /projects/novabreed/share/mvidotto/bioinfotree/binary/appliedgenomics/local/stow/* assembler:/mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/binary/appliedgenomics/local/stow/ 2>>$script_name
# sync local installation
rsync --delete -azvv -e ssh /projects/novabreed/share/mvidotto/bioinfotree/binary/appliedgenomics/prj/ltr_find/local/stow/* assembler:/mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/binary/appliedgenomics/prj/ltr_find/local/stow/ 2>>$script_name
# sync R shared libraries
rsync --delete -azvv -e ssh /projects/novabreed/share/R_libs/* assembler:/mnt/vol1/projects/novabreed/share/R_libs/ 2>>$script_name
# sync igats modules
rsync --delete -azvv -e ssh /iga/scripts/modules/* assembler:/mnt/vol1/iga/scripts/modules/ 2>>$script_name
# sync igats packages of aforementioned modules
rsync --delete -azvv -e ssh /iga/scripts/packages/* assembler:/mnt/vol1/iga/scripts/packages/ 2>>$script_name
